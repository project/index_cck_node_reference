=== Index CCK Node Reference ====

This module will insert the titles of the nodes referenced in a cck field into that node.  This is helpful for allowing deep searching of nodes with node reference fields.

Required modules:
*Search or Fuzzy Search
*Content (CCK)
*Node Reference


=== About the Author ===

Developed by Blake Lucchesi
username: blucches
blake@boldsource.com
www.boldsource.com